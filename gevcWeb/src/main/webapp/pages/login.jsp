<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<form action="/login" method="post" class="form-signin">
	<img class="mb-4 rounded-circle" src="resources/img/vitaCani.jpeg"
		alt="" width="150" height="150">
	<h1 class="h3 mb-3 font-weight-normal section-heading">Effettua l'accesso</h1>
	<div class="form-group">
		<label class="sr-only">Email</label> <input type="text"
			name="username" class="form-control" placeholder="Email" required
			autofocus>
	</div>
	<div class="form-group">
		<label class="sr-only">Password</label> <input type="password"
			name="password" class="form-control" placeholder="Password" required>
	</div>
	<input type="hidden" name="${_csrf.parameterName}"
		value="${_csrf.token}" />

	<button class="btn btn-lg btn-primary btn-block mb-1" type="submit">ACCEDI</button>

	<c:if test="${param.error ne null}">
		<p class="mt-2" style="color: red">Username e password non sono
			corrette</p>
	</c:if>
	<a href="register">Non sei registrato? Clicca qui</a>
	<p class="mt-4 text-muted">&copy; 2017-2018</p>
</form>