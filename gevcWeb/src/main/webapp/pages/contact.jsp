<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<section id="services">
	<div class="main-breadcrumb primary-color">
		<a href="contacts"><i class="breadcrumb-icon material-icons">arrow_back</i></a>
		<h5 class="breadcrumb-text">${cliente.nome}${cliente.cognome}</h5>
	</div>
	<div class="container" style="padding-top: 10px;">
		<div class="contact-element">
			<a href="tel:${cliente.cellulare}"> <i
				class="material-icons contact-icon">call</i>
				<p>${cliente.cellulare}</p>
			</a>
		</div>
		<div class="contact-element">
			<a href="mailto:${cliente.email}"> <i
				class="material-icons contact-icon">email</i>
				<p>${cliente.email}</p>
			</a>
		</div>
		<div class="contact-element">
			<a href="geo:${cliente.indirizzo}"> <i
				class="material-icons contact-icon">location_on</i>
				<p>${cliente.indirizzo}</p>
			</a>
		</div>
	</div>

	<div class="container" style="padding-top: 2rem;">
		<h5 class="contact-list-cani">I miei cani</h5>
		<ul class="list-group">
			<c:forEach items="${cliente.cani}" var="cane">
				<li class="list-group-item"><div>
						<span>Nome: ${cane.nome}</span><br>
						<c:if test="${not empty cane.razza}">
							<span>Razza: ${cane.razza.nome}</span><br>
						</c:if>
						<c:if test="${not empty cane.taglia}">
							<span>Taglia: ${cane.taglia}</span><br>
						</c:if>
						<c:if test="${not empty cane.dataNascita}">
							<fmt:formatDate value="${cane.dataNascita}" pattern="dd/MM/yyyy" var="dataNascita" />
							<span>Data di Nascita: ${dataNascita}</span><br>
						</c:if>
						<c:if test="${not empty cane.tipoPelo}">
							<span>Tipo Pelo: ${cane.tipoPelo}</span>
						</c:if>
					</div>
					<div class="clearfix"></div></li>
			</c:forEach>
		</ul>
		<button id="btnAddCane" class="btn btn-primary text-uppercase"
			type="button" data-toggle="modal" data-target="#addContactModal">Aggiungi</button>
	</div>

</section>

<!-- Modal 1 -->
<div class="portfolio-modal modal fade" id="addContactModal"
	tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="close-modal" data-dismiss="modal">
				<div class="lr">
					<div class="rl"></div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-lg-8 mx-auto">
						<div class="modal-body">
							<!-- Project Details Go Here -->
							<h3 class="text-uppercase" style="padding-top: 10px;">Aggiungi
								Cane</h3>
							<form:form method="POST" action="/cane/addCane/${cliente.id}"
								modelAttribute="cane">
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											<form:input class="form-control" path="nome" type="text"
												placeholder="Nome *" required="required"
												data-validation-required-message="Perpiacere inserisci il nome del cane." />
										</div>
										<div class="form-group mb-1">
											<form:hidden id="razzaHidden" path="razza" />
											<input class="form-control" type="text" id="input-razze"
												placeholder="Razza" autocomplete="off"> <input
												type="checkbox" class="form-check-input" id="check-razze">
											<label class="form-check-label" for="check-razze">Spunta
												se Meticcio</label>
										</div>
										<div class="form-group">
											<form:select class="form-control" path="taglia"
												required="required"
												data-validation-required-message="Perpiacere inserisci la taglia del cane.">
												<form:option value="-" label="Taglia *" />
												<form:options items="${taglie}" />
											</form:select>
										</div>
										<div class="form-group">
											<form:input class="form-control" path="dataNascita"
												type="date" placeholder="Data di Nascita *"
												required="required"
												data-validation-required-message="Perpiacere inserisci la data di nascita." />
										</div>
										<div class="form-group mb-1">
											<form:select class="form-control" path="tipoPelo"
												required="required"
												data-validation-required-message="Perpiacere inserisci la tipologia di pelo del cane.">
												<form:option value="-" label="Tipo Pelo *" />
												<form:options items="${tipiPelo}" />
											</form:select>
										</div>
										<p class="mb-1">* campo obbligatorio</p>
									</div>
									<div class="col-lg-12 text-center">
										<div id="success"></div>
										<button id="sendMessageButton"
											class="btn btn-primary btn-xl text-uppercase" type="submit">Salva</button>
									</div>
								</div>
							</form:form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="resources/js/contacts.js"></script>