<!-- Modal -->
<div class="modal fade" id="alertModal" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="alert alert-success mb-0" role="alert" id="alertType">
				<h4 class="alert-heading" id="alertTitle">Well done!</h4>
				<p id="alertText">Aww yeah, you successfully read this important alert message.
					This example text is going to run a bit longer so that you can see
					how spacing within an alert works with this kind of content.</p>
				<button id="alertButton" type="button" class="btn float-right" data-dismiss="modal">CHIUDI</button>
			</div>
		</div>
	</div>
</div>