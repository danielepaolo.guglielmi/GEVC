<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
	<div class="container">
		<a class="navbar-brand js-scroll-trigger" href="home">Vita Da Cani</a>
		<button class="navbar-toggler navbar-toggler-right" type="button"
			data-toggle="collapse" data-target="#navbarResponsive"
			aria-controls="navbarResponsive" aria-expanded="false"
			aria-label="Toggle navigation">
			Menu <i class="fa fa-bars"></i>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav text-uppercase ml-auto">
				<sec:authorize access="isAuthenticated()">
					<sec:authorize access="hasRole('ROLE_ADMIN')">
						<li class="nav-item"><a class="nav-link js-scroll-trigger"
							href="calendar">Calendario</a></li>
					</sec:authorize>
					<sec:authorize access="hasRole('ROLE_ADMIN')">
						<li class="nav-item"><a class="nav-link js-scroll-trigger"
							href="contacts">Clienti</a></li>
					</sec:authorize>
					<li class="nav-item"><a class="nav-link js-scroll-trigger"
						href="booking">Prenotazioni</a></li>
					<li class="nav-item"><a class="nav-link js-scroll-trigger"
						href="<c:url value="/logout" />">Esci</a></li>
				</sec:authorize>
				<sec:authorize access="!isAuthenticated()">
					<li class="nav-item"><a class="nav-link js-scroll-trigger"
						href="<c:url value="/login" />">Accedi</a></li>
				</sec:authorize>
			</ul>
		</div>
	</div>
</nav>