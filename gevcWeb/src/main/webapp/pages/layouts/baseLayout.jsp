<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<title><tiles:insertAttribute name="title" ignore="true" /></title>

<!--     <meta charset="utf-8"> -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<!-- Bootstrap core CSS -->
<link href="resources/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<script src="resources/vendor/jquery/jquery.min.js"></script>

<!-- Custom fonts for this template -->
<link href="resources/vendor/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700"
	rel="stylesheet" type="text/css">
<link href='https://fonts.googleapis.com/css?family=Kaushan+Script'
	rel='stylesheet' type='text/css'>
<link
	href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic'
	rel='stylesheet' type='text/css'>
<link
	href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700'
	rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">

<!-- Custom styles for this template -->
<link href="resources/css/utils/agency.min.css" rel="stylesheet">

<link href="resources/css/main.css" rel="stylesheet">
<link href="resources/css/contacts.css" rel="stylesheet">
<link href="resources/css/calendar.css" rel="stylesheet">
<link href="resources/css/booking.css" rel="stylesheet">


</head>

<body>
	<tiles:insertAttribute name="header" />
	<tiles:insertAttribute name="body" />
	<tiles:insertAttribute name="footer" />

	<jsp:include page="/pages/template/modals.jsp"></jsp:include>

	<!-- Bootstrap core JavaScript -->
	<script src="resources/js/utils/jquery.autocomplete.js"></script>
	<script src="resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Plugin JavaScript -->
	<script src="resources/vendor/jquery-easing/jquery.easing.min.js"></script>

	<!-- Contact form JavaScript -->
	<script src="resources/js/utils/jqBootstrapValidation.js"></script>
	<script src="resources/js/utils/contact_me.js"></script>

	<!-- Custom scripts for this template -->
	<script src="resources/js/utils/agency.min.js"></script>

	<!-- Custom scripts for this template -->
	<script src="resources/js/application.js"></script>
	<script src="resources/js/application_utils.js"></script>
	<script src="resources/js/init.js"></script>
</body>

</html>
