<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<section id="services">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h2 class="section-heading text-uppercase mt-2 mb-4">Prenotazione</h2>
			</div>
		</div>

		<div id="bookingCarousel" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<li data-target="bookingCarousel" data-slide-to="0" class="active"></li>
				<li data-target="bookingCarousel" data-slide-to="1"></li>
			</ol>
			<div class="carousel-inner">
				<div class="carousel-item active">
					Seleziona la data:
					<div id="my-calendar" class="mt-3"></div>
				</div>
				<div class="carousel-item">
					Seleziona la fascia oraria:
					<table class="table table-hover text-center mt-3">
						<thead>
							<tr>
								<td colspan="2"><span id="calendar_date_booking"></span></td>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${fasceOrarie }" var="fasciaOraria">
								<tr class="rowBook_fasciaOraria">
									<input type="hidden" value="${fasciaOraria.id}">
									<fmt:formatDate value="${fasciaOraria.start}" pattern="HH:mm"
										var="foStart" />
									<fmt:formatDate value="${fasciaOraria.end}" pattern="HH:mm"
										var="foEnd" />
									<td>${foStart }</td>
									<td>${foEnd }</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<div class="carousel-item">
					Seleziona il cane:
					<table class="table table-hover text-center mt-3">
						<tbody>
							<c:forEach items="${utente.cani }" var="cane">
								<tr class="rowBook_cane">
									<input type="hidden" value="${cane.id}">
									<td>${cane.nome }</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div style="overflow: auto;" class="mt-3">
			<div style="float: right;">
				<button class="btn text-uppercase" type="button" id="prevBtn"
					onclick="prevStepBooking()" style="display: none;">Indietro</button>
				<button class="btn text-uppercase" type="button" id="nextBtn"
					onclick="nextStepBooking()">Avanti</button>
			</div>
		</div>
	</div>

</section>

<!-- Modal -->
<div class="modal fade" id="bookConfirmModal" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="alert alert-success mb-0" role="alert">
				<h4 class="alert-heading">Confermi la prenotazione?</h4>
				<p>Aww yeah, you successfully read this important
					alert message. This example text is going to run a bit longer so
					that you can see how spacing within an alert works with this kind
					of content.</p>
				<form:form id="bookingForm" method="POST"
					action="/appuntamento/bookAppointment"
					modelAttribute="appuntamento">
					<form:hidden path="idCliente" value="${utente.id }" />
					<form:hidden id="form_idCane" path="idCane" />
					<form:hidden id="form_dataAppuntamento" path="dataAppuntamento" />
					<form:hidden id="form_idFasciaOraria" path="idFasciaOraria" />
					<button type="button" class="btn float-right"
						data-dismiss="modal">OK</button>
					<button type="button" class="btn float-right"
						data-dismiss="modal">ANNULLA</button>
				</form:form>
			</div>
		</div>
	</div>
</div>

<script src="resources/js/booking.js"></script>
<script src="resources/js/calendar.js"></script>