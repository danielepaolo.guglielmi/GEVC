<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<form:form method="POST" action="/cliente/registraCliente"
	class="form-signin" modelAttribute="registrationForm">
	<img class="mb-4 rounded-circle" src="resources/img/vitaCani.jpeg"
		alt="" width="120" height="120">
	<h1 class="h3 mb-3 font-weight-normal section-heading">Registrati</h1>
	<div class="form-group mb-1">
		<form:input class="form-control" path="nome" type="text"
			placeholder="Nome *" required="required"
			data-validation-required-message="Perpiacere inserisci il tuo nome." />
	</div>
	<div class="form-group mb-1">
		<form:input class="form-control" path="cognome" type="text"
			placeholder="Cognome *" required="required"
			data-validation-required-message="Perpiacere inserisci il tuo cognome." />
	</div>
	<div class="form-group mb-1">
		<form:input class="form-control" path="dataNascita" type="date"
			placeholder="Data di Nascita *" required="required"
			data-validation-required-message="Perpiacere inserisci la data di nascita." />
	</div>
	<div class="form-group mb-1">
		<form:input class="form-control" path="cellulare" type="tel"
			placeholder="Cellulare *" required="required"
			data-validation-required-message="Perpiacere inserisci il tuo numero di telefono." />
	</div>
	<div class="form-group mb-1">
		<form:input class="form-control" path="indirizzo" type="text"
			placeholder="Indirizzo *" required="required"
			data-validation-required-message="Perpiacere inserisci il tuo indirizzo." />
	</div>
	<div class="form-group mb-1">
		<form:input class="form-control" path="email" type="email"
			placeholder="Email *" required="required"
			data-validation-required-message="Perpiacere inserisci l'email." />
	</div>
	<div class="form-group mb-1">
		<input class="form-control" type="password"
			placeholder="Password *" required="required"
			data-validation-required-message="Perpiacere inserisci la password." >
	</div>
	<div class="form-group mb-1">
		<form:input class="form-control" path="password" type="password"
			placeholder="Ripeti Password *" required="required"
			data-validation-required-message="Perpiacere inserisci la password." />
	</div>

	<button class="btn btn-lg btn-primary btn-block mt-3" type="submit">REGISTRATI</button>

	<c:if test="${param.error ne null}">
		<p class="mt-2" style="color: red">Sei spacciato.</p>
	</c:if>
	<p class="mt-4 text-muted">&copy; 2017-2018</p>
</form:form>