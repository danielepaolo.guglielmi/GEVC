<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<section id="services">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h2 class="section-heading text-uppercase mt-2">Contatti</h2>
			</div>
		</div>

		<div class="row mb-3">
			<div class="col-md-1 col-lg-2"></div>
			<div class="col-md-10 col-lg-8">
				<div class="input-group">
					<input class="form-control" type="text" id="search-cliente"
						onkeyup="searchCliente()" placeholder="Cerca...">
					<div class="input-group-append">
						<span class="input-group-text"><i class="material-icons">search</i></span>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-1 col-lg-2"></div>
			<div class="col-md-10 col-lg-8">
				<ul class="list-group" id="contact-list">
					<c:forEach items="${clienti}" var="cliente">
						<form method="GET" action="/contact">
							<input type="hidden" name="idCliente" value="${cliente.id}" />
							<button class="list-group-item contact-group-item" type="submit">
								<img src="http://api.randomuser.me/portraits/women/90.jpg"
									alt="${cliente.nome} ${cliente.cognome}"
									class="img-responsive img-circle mr-3">
								<div>
									<span class="visible-xs"> <span class="text-muted">${cliente.nome}
											${cliente.cognome}</span><br></span><span class="visible-xs"> <span
										class="text-muted">${cliente.indirizzo}</span><br></span> <span
										class="visible-xs"> <span class="text-muted">${cliente.cellulare}</span><br></span>
								</div>
								<%-- <span class="fa fa-comments text-muted c-info"
									data-toggle="tooltip" title="${cliente.email}"></span> <span
									class="visible-xs"> <span class="text-muted">${cliente.email}</span><br /></span> --%>
								<div class="clearfix"></div>
							</button>
						</form>
					</c:forEach>
				</ul>
			</div>
		</div>
	</div>

</section>

<button type="button" class="float" data-toggle="modal"
	data-target="#addContactModal">
	<i class="fa fa-plus my-float"></i>
</button>

<!-- Modal 1 -->
<div class="portfolio-modal modal fade" id="addContactModal"
	tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="close-modal" data-dismiss="modal">
				<div class="lr">
					<div class="rl"></div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-lg-8 mx-auto">
						<div class="modal-body">
							<!-- Project Details Go Here -->
							<h3 class="text-uppercase" style="padding-top: 10px;">Crea
								Contatto</h3>
							<form:form method="POST" action="/cliente/addCliente"
								modelAttribute="cliente">
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											<form:input class="form-control" path="nome" type="text"
												placeholder="Nome *" required="required"
												data-validation-required-message="Perpiacere inserisci il tuo nome." />
										</div>
										<div class="form-group">
											<form:input class="form-control" path="cognome" type="text"
												placeholder="Cognome *" required="required"
												data-validation-required-message="Perpiacere inserisci il tuo cognome." />
										</div>
										<div class="form-group">
											<form:input class="form-control" path="dataNascita"
												type="date" placeholder="Data di Nascita *"
												required="required"
												data-validation-required-message="Perpiacere inserisci la data di nascita." />
										</div>
										<div class="form-group">
											<form:input class="form-control" path="cellulare" type="tel"
												placeholder="Cellulare *" required="required"
												data-validation-required-message="Perpiacere inserisci il tuo numero di telefono." />
										</div>
										<div class="form-group">
											<form:input class="form-control" path="email" type="email"
												placeholder="Email *" required="required"
												data-validation-required-message="Perpiacere inserisci l'email." />
										</div>
										<div class="form-group mb-1">
											<form:input class="form-control" path="indirizzo" type="text"
												placeholder="Indirizzo *" required="required"
												data-validation-required-message="Perpiacere inserisci il tuo indirizzo." />
										</div>
										<p class="mb-1">* campo obbligatorio</p>
									</div>
									<div class="col-lg-12 text-center">
										<div id="success"></div>
										<button id="sendMessageButton"
											class="btn btn-primary btn-xl text-uppercase" type="submit">Salva</button>
									</div>
								</div>
							</form:form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="resources/js/contacts.js"></script>