<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<section id="services">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h2 class="section-heading text-uppercase mt-2 mb-4">Calendario</h2>
			</div>
		</div>

		<div id="calendarCarousel" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<li data-target="calendarCarousel" data-slide-to="0" class="active"></li>
				<li data-target="calendarCarousel" data-slide-to="1"></li>
			</ol>
			<div class="carousel-inner">
				<div class="carousel-item active">
					Seleziona la data:
					<div id="my-calendar" class="mt-3"></div>
				</div>
				<div class="carousel-item">
					Seleziona la fascia oraria:
					<table class="table table-hover text-center mt-3">
						<thead>
							<tr>
								<td colspan="2"><span id="calendar_date_booking"></span></td>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${fasceOrarie }" var="fasciaOraria">
								<tr>
									<input type="hidden" id="foCalendar-${fasciaOraria.id}" value="${fasciaOraria.id}">
									<fmt:formatDate value="${fasciaOraria.start}" pattern="HH:mm" var="foStart" />
									<fmt:formatDate value="${fasciaOraria.end}" pattern="HH:mm" var="foEnd" />
									<td>${foStart } - ${foEnd }</td>
									<td>Otto</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div style="overflow: auto;" class="mt-3">
			<div style="float: right;">
				<button class="btn text-uppercase" type="button" id="prevBtn"
					onclick="prevStepCalendar()" style="display: none;">Indietro</button>
				<button class="btn text-uppercase" type="button" id="nextBtn"
					onclick="nextStepCalendar()">Avanti</button>
			</div>
		</div>
	</div>

</section>

<script src="resources/js/booking.js"></script>
<script src="resources/js/calendar.js"></script>