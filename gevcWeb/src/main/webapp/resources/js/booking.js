var month_labels = [ "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio",
		"Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre",
		"Dicembre" ];

var bcPages = 3;
var bcCurrPage = 0;

function nextStepBooking() {
	if(bcCurrPage < bcPages) {
		bcCurrPage++;
	} else {
		$('#bookConfirmModal').modal('show');
	}
	
	if(bcCurrPage == 2) {
		$("#nextBtn").text("Prenota");
	}
	
	$("#bookingCarousel").carousel(bcCurrPage);
	$("#prevBtn").show();
}

function prevStepBooking() {
	if(bcCurrPage > 0) {
		bcCurrPage--;
	} else {
		
	}
	
	if(bcCurrPage == 0) {
		$("#prevBtn").hide();
	}
	
	$("#bookingCarousel").carousel(bcCurrPage);
	$("#nextBtn").text("Avanti");
}

function nextStepCalendar() {
	$("#calendarCarousel").carousel(1);
	$("#prevBtn").show();
	$("#nextBtn").hide();
}

function prevStepCalendar() {
	$("#calendarCarousel").carousel(0);
	$("#prevBtn").hide();
	$("#nextBtn").show();
}

function onDayClickBook(id) {
	var date = $("#" + id).data("date");
	var isPast = $("#" + id).children().hasClass("past");
	if (!isPast) {
		$("div").removeClass("day-selected");
		$("#" + id).children("div").addClass("day-selected");
		var splittedDate = date.split("-");
		$('#calendar_date_booking').text(
				splittedDate[2] + ' ' + month_labels[parseInt(splittedDate[1])]
						+ ' ' + splittedDate[0]);
		if (document.getElementById('bookingCarousel') != null) {
			$('#form_dataAppuntamento').val(date);
			nextStepBooking();
		}
		if (document.getElementById('calendarCarousel') != null) {
			nextStepCalendar();
		}
		var response = ajaxGetCall("/appuntamento/getAppointmentByData/"
				+ splittedDate[2] + splittedDate[1] + splittedDate[0]);
		console.log(response);
	}
}

$(".rowBook_fasciaOraria").on("click", function() {
	if (document.getElementById('bookingCarousel') != null) {
		var idFasciaOraria = $(this).find("input").val();
		$('#form_idFasciaOraria').val(idFasciaOraria);
		nextStepBooking();
	}
});

$(".rowBook_cane").on("click", function() {
	if (document.getElementById('bookingCarousel') != null) {
		var idFasciaOraria = $(this).find("input").val();
		$('#form_idCane').val(idFasciaOraria);
		nextStepBooking();
	}
});