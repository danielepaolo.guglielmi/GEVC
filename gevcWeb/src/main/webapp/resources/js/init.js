$(document).ready(function() {

	$('#input-razze').autocomplete({
		serviceUrl : '/razza_canina/getSuggestRazzeFci',
		paramName : "razza",
		delimiter : ",",
		transformResult : function(response) {
			return {
				// must convert json to javascript object before
				// process
				suggestions : $.map($.parseJSON(response), function(item) {
					return {
						value : item.nome,
						data : item.id
					};
				})

			};

		},
		onSelect : function(suggestion) {
			$("#razzaHidden").val(suggestion.data);
		}

	});

	$("#my-calendar").zabuto_calendar({
		action : function() {
			onDayClickBook(this.id);
		},
		language : "it",
		show_previous : false,
		show_next : 3,
		show_days : true
	});

	$('.carousel').carousel({
		interval : 0,
		pause : true
	})

});

function CustomAlert() {
	this.render = function(dialog, type, title) {
		$('#alertType').removeClass('alert-success').removeClass('alert-info')
				.removeClass('alert-warning').removeClass('alert-danger');
		$('#alertButton').removeClass('btn-success').removeClass('btn-info')
				.removeClass('btn-warning').removeClass('btn-danger');
		if (type == 'warning') {
			$('#alertTitle').html("Attenzione");
			$('#alertType').addClass('alert-warning');
			$('#alertButton').addClass('alert-warning');
		} else if (type == 'error') {
			$('#alertTitle').html("Ops Errore");
			$('#alertType').addClass('alert-danger');
			$('#alertButton').addClass('btn-danger');
		} else if (type == 'info') {
			$('#alertTitle').html(title);
			$('#alertType').addClass('alert-info');
			$('#alertButton').addClass('btn-info');
		}
		$('#alertText').html(dialog);

		$('#alertModal').modal('show');
	}
	this.ok = function() {
		$('#alertDialogBox').hide();
		$('#alertDialogOverlay').hide();
		$('#alertDialog').removeAttr('style');
	}
}
var Alert = new CustomAlert();