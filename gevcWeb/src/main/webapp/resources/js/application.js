$("#check-razze").change(function() {
	if (this.checked) {
		$("#input-razze").prop('disabled', true);
		$("#input-razze").val('');
		$("#razzaHidden").val('');
	} else {
		$("#input-razze").prop('disabled', false);
		$("#razzaHidden").val('');
	}
});
