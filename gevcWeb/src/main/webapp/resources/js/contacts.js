function searchCliente() {
	var input, filter, list, button, name;
	var input = document.getElementById("search-cliente");
	filter = input.value.toUpperCase();
	list = document.getElementById("contact-list");
	button = list.getElementsByTagName("button");
	for (i = 0; i < button.length; i++) {
		name = button[i].getElementsByClassName("name")[0].innerHTML;
		if (name.toUpperCase().indexOf(filter) > -1) {
			button[i].style.display = "";
		} else {
			button[i].style.display = "none";
		}
	}
}