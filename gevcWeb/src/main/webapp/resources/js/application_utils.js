// ********************************************
// ************** CHIAMATE AJAX ***************
// ********************************************
function ajaxGetCall(url) {
	var response;

	$.ajax({
		type : "GET",
		cache : false,
		async : false,
		url : url,
		contentType : "application/json",
		success : function(dati) {
			response = dati;
		},
		error : function(errMsg) {
			Alert.render("Errore nell'invocazione del servizio", 'error', null);

			response = null;
		}
	});

	return response;
}