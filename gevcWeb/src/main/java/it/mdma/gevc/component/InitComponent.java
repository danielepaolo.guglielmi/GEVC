package it.mdma.gevc.component;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import it.mdma.gevc.backend.service.ICaneService;
import it.mdma.gevc.backend.service.IClienteService;
import it.mdma.gevc.backend.service.IFasciaOrariaService;
import it.mdma.gevc.backend.service.IGruppoFCIService;
import it.mdma.gevc.backend.service.IRazzaFCIService;
import it.mdma.gevc.backend.service.ISezioneFCIService;
import it.mdma.gevc.config.access.Role;
import it.mdma.gevc.config.access.User;
import it.mdma.gevc.config.access.UserRepository;
import it.mdma.gevc.model.Cane;
import it.mdma.gevc.model.Cliente;
import it.mdma.gevc.model.FasciaOraria;
import it.mdma.gevc.model.GruppoFCI;
import it.mdma.gevc.model.RazzaFCI;
import it.mdma.gevc.model.SezioneFCI;
import it.mdma.gevc.utility.ApplicationConstants;

@Component
public class InitComponent {

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Autowired
	private UserRepository userRepository;

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ServletContext context;

	@SuppressWarnings("unchecked")
	private List<FasciaOraria> selectAllFasceOrarie() {
		return (List<FasciaOraria>) context.getAttribute(ApplicationConstants.CONTEXT_FASCE_ORARIE);
	}

	@SuppressWarnings("unchecked")
	public List<GruppoFCI> selectAllGruppiFCI() {
		return (List<GruppoFCI>) context.getAttribute(ApplicationConstants.CONTEXT_GRUPPI_FCI);
	}

	@SuppressWarnings("unchecked")
	public List<SezioneFCI> selectAllSezioniFCI() {
		return (List<SezioneFCI>) context.getAttribute(ApplicationConstants.CONTEXT_SEZIONI_FCI);
	}

	@SuppressWarnings("unchecked")
	public List<RazzaFCI> selectAllRazzeFCI() {
		return (List<RazzaFCI>) context.getAttribute(ApplicationConstants.CONTEXT_RAZZE_FCI);
	}

	@Autowired
	private IFasciaOrariaService fasciaOrariaService;

	@Autowired
	private IGruppoFCIService gruppoFCIService;

	@Autowired
	private ISezioneFCIService sezioneFCIService;

	@Autowired
	private IRazzaFCIService razzaFCIService;

	@Autowired
	private IClienteService clienteService;

	@Autowired
	private ICaneService caneService;

	@PostConstruct
	public void init() {
		fasciaOrariaService.insertAll(selectAllFasceOrarie());
		gruppoFCIService.insertAll(selectAllGruppiFCI());
		sezioneFCIService.insertAll(selectAllSezioniFCI());
		razzaFCIService.insertAll(selectAllRazzeFCI());

		User user = new User("danielgugo@gmail.com", passwordEncoder.encode("admin"),
				Arrays.asList(new Role("ROLE_USER"), new Role("ROLE_ADMIN")));

		if (userRepository.findByEmail(user.getEmail()) == null) {
			userRepository.save(user);
		}

		Cliente dani = new Cliente();
		dani.setNome("Daniele");
		dani.setCognome("Guglielmi");
		dani.setCellulare("3387642638");
		dani.setEmail("danielgugo@gmail.com");
		dani.setIndirizzo("via Giacomo Tauro 3/B");
		dani.setDataNascita(new Date());

		Cane rufy = new Cane();
		rufy.setNome("Rufy");
		rufy.setCliente(dani);
		dani.getCani().add(rufy);

		clienteService.insert(dani);

		Cliente retDani = clienteService.selectById(new Long(1));

		log.info(retDani.toString());
		log.info(retDani.getCani().get(0).toString());

		Cane gio = new Cane();
		gio.setNome("Giovane");
		gio.setCliente(retDani);

		caneService.insert(gio);

		Cane retRufy = caneService.selectById(new Long(1));

		log.info(retRufy.toString());
		log.info(retRufy.getCliente().toString());

		log.info("DB popolato");

	}

}
