package it.mdma.gevc.backend.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.mdma.gevc.backend.repository.GruppoFCIRepository;
import it.mdma.gevc.backend.service.IGruppoFCIService;
import it.mdma.gevc.model.GruppoFCI;

@Service
public class GruppoFCIServiceImpl implements IGruppoFCIService {
	
	@Autowired
	private GruppoFCIRepository gruppoFCIRepository;

	public List<GruppoFCI> selectAll() {
		return gruppoFCIRepository.findAll();
		
	}

	public List<GruppoFCI> insertAll(List<GruppoFCI> gruppoFci) {
		return gruppoFCIRepository.saveAll(gruppoFci);
		
	}

}
