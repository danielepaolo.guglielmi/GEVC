package it.mdma.gevc.backend.service;

import java.util.List;

import it.mdma.gevc.model.SezioneFCI;

public interface ISezioneFCIService {
	public List<SezioneFCI> selectAll();
	public List<SezioneFCI> insertAll(List<SezioneFCI> sezioniFci);
	
}
