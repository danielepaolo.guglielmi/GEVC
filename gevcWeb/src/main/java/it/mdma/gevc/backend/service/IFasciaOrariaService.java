package it.mdma.gevc.backend.service;

import java.util.List;

import it.mdma.gevc.model.FasciaOraria;

public interface IFasciaOrariaService {
	public List<FasciaOraria> selectAll();
	public List<FasciaOraria> insertAll(List<FasciaOraria> fasceOrarie);
	
}
