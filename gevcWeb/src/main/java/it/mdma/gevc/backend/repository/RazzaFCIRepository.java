package it.mdma.gevc.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import it.mdma.gevc.model.RazzaFCI;

public interface RazzaFCIRepository extends JpaRepository<RazzaFCI, Long> {

}
