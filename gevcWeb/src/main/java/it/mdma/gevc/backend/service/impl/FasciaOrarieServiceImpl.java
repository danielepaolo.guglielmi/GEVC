package it.mdma.gevc.backend.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.mdma.gevc.backend.repository.FasciaOrariaRepository;
import it.mdma.gevc.backend.service.IFasciaOrariaService;
import it.mdma.gevc.model.FasciaOraria;

@Service
public class FasciaOrarieServiceImpl implements IFasciaOrariaService {
	
	@Autowired
	private FasciaOrariaRepository fasciaOrariaRepository;

	public List<FasciaOraria> selectAll() {
		return fasciaOrariaRepository.findAll();
		
	}

	public List<FasciaOraria> insertAll(List<FasciaOraria> fasceOrarie) {
		return fasciaOrariaRepository.saveAll(fasceOrarie);
		
	}

}
