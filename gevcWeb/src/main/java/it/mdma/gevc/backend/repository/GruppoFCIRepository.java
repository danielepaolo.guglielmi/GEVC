package it.mdma.gevc.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import it.mdma.gevc.model.GruppoFCI;

public interface GruppoFCIRepository extends JpaRepository<GruppoFCI, Long> {

}
