package it.mdma.gevc.backend.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import it.mdma.gevc.backend.exception.ResourceNotFoundException;
import it.mdma.gevc.backend.repository.AppuntamentoRepository;
import it.mdma.gevc.backend.service.IAppuntamentoService;
import it.mdma.gevc.model.Appuntamento;

@Service
public class AppuntamentoServiceImpl implements IAppuntamentoService {

	@Autowired
	private AppuntamentoRepository appuntamentoRepository;

	public List<Appuntamento> selectAll() {
		return appuntamentoRepository.findAll();

	}
	
	public Appuntamento insert(Appuntamento agenda) {
		return appuntamentoRepository.save(agenda);

	}

	public ResponseEntity<?> delete(Long id) {
		Appuntamento agenda = appuntamentoRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Agenda", "id", id));

		appuntamentoRepository.delete(agenda);

		return ResponseEntity.ok().build();
		
	}

	public List<Appuntamento> selectByDataAppuntamento(Date dataAppuntamento){
		return appuntamentoRepository.findByDataAppuntamento(dataAppuntamento);
		
	}

}
