package it.mdma.gevc.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import it.mdma.gevc.model.SezioneFCI;

public interface SezioneFCIRepository extends JpaRepository<SezioneFCI, Long> {

}
