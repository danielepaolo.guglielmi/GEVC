package it.mdma.gevc.backend.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import it.mdma.gevc.backend.exception.ResourceNotFoundException;
import it.mdma.gevc.backend.repository.CaneRepository;
import it.mdma.gevc.backend.service.ICaneService;
import it.mdma.gevc.model.Cane;

@Service
public class CaneServiceImpl implements ICaneService {

	@Autowired
	private CaneRepository caneRepository;

	public Cane selectById(Long id) {
		return caneRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Cane", "id", id));

	}

	public List<Cane> selectByIdCliente(Long idCliente) {
//		Cane cane = new Cane();
//		cane.setIdCliente(idCliente);
//		Example<Cane> example = Example.of(cane);
//		return caneRepository.findAll(example);
		return null;

	}

	public Cane insert(Cane cane) {
		return caneRepository.save(cane);

	}

	public Cane update(Long id, Cane caneUpdated) {
		Cane cane = caneRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Cane", "id", id));

		if (!caneUpdated.getNome().isEmpty()) {
			cane.setNome(caneUpdated.getNome());
		}
		if (caneUpdated.getRazza() != null) {
			cane.setRazza(caneUpdated.getRazza());
		}
		if (!caneUpdated.getTaglia().isEmpty()) {
			cane.setTaglia(caneUpdated.getTaglia());
		}
		if (caneUpdated.getDataNascita() != null) {
			cane.setDataNascita(caneUpdated.getDataNascita());
		}
		if (!caneUpdated.getTipoPelo().isEmpty()) {
			cane.setTipoPelo(caneUpdated.getTipoPelo());
		}

		return caneRepository.save(cane);

	}

	public ResponseEntity<?> delete(Long id) {
		Cane cane = caneRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Cane", "id", id));

		caneRepository.delete(cane);

		return ResponseEntity.ok().build();

	}

}
