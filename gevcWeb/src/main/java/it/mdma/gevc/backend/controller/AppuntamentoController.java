package it.mdma.gevc.backend.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.mdma.gevc.backend.exception.GenericException;
import it.mdma.gevc.backend.service.IAppuntamentoService;
import it.mdma.gevc.model.Appuntamento;

@RequestMapping("/appuntamento")
@RestController
public class AppuntamentoController {

	@Autowired
	private IAppuntamentoService appuntamentoService;

	@GetMapping("/getAllAppointment")
	public List<Appuntamento> getAllAppointment() {
		return appuntamentoService.selectAll();

	}

	@PostMapping("/bookAppointment")
	public Appuntamento bookAppointment(@Valid @RequestBody Appuntamento appuntamento) {
		return appuntamentoService.insert(appuntamento);

	}

	@GetMapping("/cancelAppointment/{id}")
	public ResponseEntity<?> cancelAppointment(@PathVariable(value = "id") Long id) {
		return appuntamentoService.delete(id);

	}

	@GetMapping("/getAppointmentByData/{data}")
	public List<Appuntamento> getAppointmentByData(@PathVariable(value = "data") Long data) {
		DateFormat df = new SimpleDateFormat("ddMMyyyy");
		Date result = null;
	    try {
			result =  df.parse(Long.toString(data));
		} catch (ParseException e) {
			throw new GenericException("Parametro data non corretto");
		} 
		return appuntamentoService.selectByDataAppuntamento(result);

	}

}
