package it.mdma.gevc.backend.service;

import java.util.List;

import it.mdma.gevc.model.RazzaFCI;

public interface IRazzaFCIService {
	public List<RazzaFCI> selectAll();
	public List<RazzaFCI> insertAll(List<RazzaFCI> razzeFci);
	
}
