package it.mdma.gevc.backend.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.mdma.gevc.backend.repository.RazzaFCIRepository;
import it.mdma.gevc.backend.service.IRazzaFCIService;
import it.mdma.gevc.model.RazzaFCI;

@Service
public class RazzaFCIServiceImpl implements IRazzaFCIService {
	
	@Autowired
	private RazzaFCIRepository razzaFCIRepository;

	public List<RazzaFCI> selectAll() {
		return razzaFCIRepository.findAll();
		
	}

	public List<RazzaFCI> insertAll(List<RazzaFCI> razzeFci) {
		return razzaFCIRepository.saveAll(razzeFci);
		
	}

}
