package it.mdma.gevc.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import it.mdma.gevc.model.Cane;

public interface CaneRepository extends JpaRepository<Cane, Long> {

}
