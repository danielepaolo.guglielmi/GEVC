package it.mdma.gevc.backend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class GenericException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8262924111464464328L;
	private String message;

	public GenericException(String message) {
		super(message);
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
