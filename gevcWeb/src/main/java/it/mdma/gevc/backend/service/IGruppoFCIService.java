package it.mdma.gevc.backend.service;

import java.util.List;

import it.mdma.gevc.model.GruppoFCI;

public interface IGruppoFCIService {
	public List<GruppoFCI> selectAll();
	public List<GruppoFCI> insertAll(List<GruppoFCI> gruppiFci);
	
}
