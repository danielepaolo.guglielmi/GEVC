package it.mdma.gevc.backend.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.mdma.gevc.backend.service.IRazzeCanineService;
import it.mdma.gevc.model.GruppoFCI;
import it.mdma.gevc.model.RazzaFCI;

@RequestMapping("/razza_canina")
@RestController
public class RazzaCaninaController {

	@Autowired
	private IRazzeCanineService razzaCaninaService;

	@GetMapping("/getSuggestRazzeFci")
	public List<RazzaFCI> getRazzeFci(@RequestParam String razza) {
		return simulateSearchResult(razza);

	}

	private List<RazzaFCI> simulateSearchResult(String razza) {
		List<RazzaFCI> result = new ArrayList<>();
		for (RazzaFCI tag : razzaCaninaService.selectAllRazzeFCI()) {
			if (tag.getNome().toLowerCase().contains(razza.toLowerCase())) {
				result.add(tag);
			}
		}

		return result;
		
	}

	@GetMapping("/getRazzeFciByGroup/{id}")
	public List<GruppoFCI> getRazzeFciByGroup(@PathVariable(value = "id") Long id) {
		return razzaCaninaService.selectRazzeFCIWhereIdGruppoIsEqual(id);

	}

}
