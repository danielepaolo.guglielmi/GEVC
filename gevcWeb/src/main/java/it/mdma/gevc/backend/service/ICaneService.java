package it.mdma.gevc.backend.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import it.mdma.gevc.model.Cane;

public interface ICaneService {
	public Cane selectById(Long id);
	public List<Cane> selectByIdCliente(Long idCliente);
	public Cane insert(Cane cane);
	public Cane update(Long id, Cane cane);
	public ResponseEntity<?> delete(Long id);
    
}