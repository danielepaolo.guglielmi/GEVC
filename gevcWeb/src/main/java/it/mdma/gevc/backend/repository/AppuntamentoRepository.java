package it.mdma.gevc.backend.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.mdma.gevc.model.Appuntamento;

@Repository
public interface AppuntamentoRepository extends JpaRepository<Appuntamento, Long> {
	public List<Appuntamento> findByDataAppuntamento(Date dataAppuntamento);
}
