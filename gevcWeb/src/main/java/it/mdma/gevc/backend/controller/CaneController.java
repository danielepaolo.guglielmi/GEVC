package it.mdma.gevc.backend.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.mdma.gevc.backend.service.ICaneService;
import it.mdma.gevc.backend.service.IClienteService;
import it.mdma.gevc.model.Cane;
import it.mdma.gevc.model.Cliente;

@RequestMapping("/cane")
@RestController
public class CaneController {

	@Autowired
	private IClienteService clienteService;

	@Autowired
	private ICaneService caneService;

	@GetMapping("/getCane/{id}")
	public Cane getCane(@PathVariable(value = "id") Long id) {
		return caneService.selectById(id);

	}

	@GetMapping("/getCaniCliente/{idCliente}")
	public List<Cane> getCaniCliente(@PathVariable(value = "idCliente") Long idCliente) {
		return caneService.selectByIdCliente(idCliente);

	}

	@PostMapping("/addCane/{idCliente}")
	public void addCane(@PathVariable(value = "idCliente") Long idCliente, @Valid @ModelAttribute("cane") Cane cane,
			HttpServletResponse response) throws IOException {
		Cliente padrone = clienteService.selectById(idCliente);
		cane.setCliente(padrone);
		caneService.insert(cane);
		response.sendRedirect("/contact?idCliente=" + idCliente);

	}

	@PostMapping("/updateCane/{id}")
	public Cane updateCane(@PathVariable(value = "id") Long id, @Valid @RequestBody Cane cane) {
		return caneService.update(id, cane);

	}

	@GetMapping("/deleteCane/{id}")
	public ResponseEntity<?> deleteCane(@PathVariable(value = "id") Long id) {
		return caneService.delete(id);

	}

}
