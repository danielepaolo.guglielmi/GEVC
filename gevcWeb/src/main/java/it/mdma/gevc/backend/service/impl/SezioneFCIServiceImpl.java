package it.mdma.gevc.backend.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.mdma.gevc.backend.repository.SezioneFCIRepository;
import it.mdma.gevc.backend.service.ISezioneFCIService;
import it.mdma.gevc.model.SezioneFCI;

@Service
public class SezioneFCIServiceImpl implements ISezioneFCIService {
	
	@Autowired
	private SezioneFCIRepository sezioneFCIRepository;

	public List<SezioneFCI> selectAll() {
		return sezioneFCIRepository.findAll();
		
	}

	public List<SezioneFCI> insertAll(List<SezioneFCI> sezioniFci) {
		return sezioneFCIRepository.saveAll(sezioniFci);
		
	}

}
