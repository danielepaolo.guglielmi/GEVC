package it.mdma.gevc.backend.service.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import it.mdma.gevc.backend.exception.GenericException;
import it.mdma.gevc.backend.exception.ResourceNotFoundException;
import it.mdma.gevc.backend.repository.ClienteRepository;
import it.mdma.gevc.backend.service.IClienteService;
import it.mdma.gevc.config.access.Role;
import it.mdma.gevc.config.access.User;
import it.mdma.gevc.config.access.UserRepository;
import it.mdma.gevc.model.Cliente;
import it.mdma.gevc.model.dto.RegistrationForm;

@Service
public class ClienteServiceImpl implements IClienteService {

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ClienteRepository clienteRepository;

	public Cliente insert(RegistrationForm registrationForm) {
		User user = new User(registrationForm.getEmail(), passwordEncoder.encode(registrationForm.getPassword()),
				Arrays.asList(new Role("ROLE_USER")));
		if (userRepository.findByEmail(user.getEmail()) == null) {
			userRepository.save(user);

			Cliente cliente = new Cliente();
			cliente.setNome(registrationForm.getNome());
			cliente.setCognome(registrationForm.getCognome());
			cliente.setCellulare(registrationForm.getCellulare());
			cliente.setEmail(registrationForm.getEmail());
			cliente.setIndirizzo(registrationForm.getIndirizzo());
			cliente.setDataNascita(registrationForm.getDataNascita());

			return clienteRepository.save(cliente);
		} else {
			throw new GenericException("Questa email è già associata ad un utente.");
		}
	}

	public List<Cliente> selectAll() {
		return clienteRepository.findAll();

	}

	public Cliente selectById(Long id) {
		return clienteRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Cliente", "id", id));

	}

	public Cliente selectByEmail(String email) {
		Cliente cliente = clienteRepository.findByEmail(email);
		if (cliente == null) {
			throw new ResourceNotFoundException("Cliente", "email", email);
		}
		return cliente;

	}

	public Cliente insert(Cliente cliente) {
		return clienteRepository.save(cliente);

	}

	public Cliente update(Long id, Cliente clienteUpdated) {
		Cliente cliente = clienteRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Cliente", "id", id));

		if (clienteUpdated.getNome() != null && !clienteUpdated.getNome().isEmpty()) {
			cliente.setNome(clienteUpdated.getNome());
		}
		if (clienteUpdated.getCognome() != null && !clienteUpdated.getCognome().isEmpty()) {
			cliente.setCognome(clienteUpdated.getCognome());
		}
		if (clienteUpdated.getCellulare() != null && !clienteUpdated.getCellulare().isEmpty()) {
			cliente.setCellulare(clienteUpdated.getCellulare());
		}
		if (clienteUpdated.getEmail() != null && !clienteUpdated.getEmail().isEmpty()) {
			cliente.setEmail(clienteUpdated.getEmail());
		}
		if (clienteUpdated.getIndirizzo() != null && !clienteUpdated.getIndirizzo().isEmpty()) {
			cliente.setIndirizzo(clienteUpdated.getIndirizzo());
		}
		if (clienteUpdated.getDataNascita() != null) {
			cliente.setDataNascita(clienteUpdated.getDataNascita());
		}

		return clienteRepository.save(cliente);

	}

	public ResponseEntity<?> delete(Long id) {
		Cliente cliente = clienteRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Cliente", "id", id));

		clienteRepository.delete(cliente);

		return ResponseEntity.ok().build();

	}

}