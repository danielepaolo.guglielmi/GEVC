package it.mdma.gevc.backend.service;

import java.util.List;

import it.mdma.gevc.model.GruppoFCI;
import it.mdma.gevc.model.RazzaFCI;

public interface IRazzeCanineService {
	// Le razze sono tutte suddivise per gruppi, poi sezioni
	public List<RazzaFCI> selectAllRazzeFCI();
	public List<GruppoFCI> selectAllRazzeFCIGroupByIdGruppo();
	public List<GruppoFCI> selectRazzeFCIWhereIdGruppoIsEqual(final Long idGruppo);
	
}
