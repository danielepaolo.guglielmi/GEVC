package it.mdma.gevc.backend.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.mdma.gevc.backend.service.IRazzeCanineService;
import it.mdma.gevc.model.GruppoFCI;
import it.mdma.gevc.model.RazzaFCI;
import it.mdma.gevc.model.SezioneFCI;
import it.mdma.gevc.utility.ApplicationConstants;

@Service
public class RazzeCanineServiceImpl implements IRazzeCanineService {

	@Autowired
	private ServletContext context;

	@SuppressWarnings("unchecked")
	private List<GruppoFCI> selectAllGruppiFCI() {
		return (List<GruppoFCI>) context.getAttribute(ApplicationConstants.CONTEXT_GRUPPI_FCI);
	}

	@SuppressWarnings("unchecked")
	private List<SezioneFCI> selectAllSezioniFCI() {
		return (List<SezioneFCI>) context.getAttribute(ApplicationConstants.CONTEXT_SEZIONI_FCI);
	}

	@SuppressWarnings("unchecked")
	public List<RazzaFCI> selectAllRazzeFCI() {
		return (List<RazzaFCI>) context.getAttribute(ApplicationConstants.CONTEXT_RAZZE_FCI);
	}

	public List<GruppoFCI> selectAllRazzeFCIGroupByIdGruppo() {
		List<GruppoFCI> gruppiFci = selectAllGruppiFCI();
		List<SezioneFCI> sezioniFci = selectAllSezioniFCI();
		List<RazzaFCI> razzeFci = selectAllRazzeFCI();

		List<GruppoFCI> result = new ArrayList<>();
		for (GruppoFCI gruppoFci : gruppiFci) {
			GruppoFCI gruppo = new GruppoFCI(gruppoFci.getId(), gruppoFci.getDescrizione());
			List<SezioneFCI> sezioni = new ArrayList<>();
			for (SezioneFCI sezioneFci : sezioniFci) {
				
				if (sezioneFci.getIdGruppo() == gruppo.getId()) {
					SezioneFCI sezione = new SezioneFCI(sezioneFci.getId(), sezioneFci.getIdGruppo(),
							sezioneFci.getDescrizione());

					List<RazzaFCI> razze = new ArrayList<>();
					for (RazzaFCI razzaFci : razzeFci) {
						
						if (razzaFci.getIdGruppo() == gruppo.getId()
								&& razzaFci.getIdSezione().equals(sezioneFci.getId())) {
							RazzaFCI razza = new RazzaFCI(razzaFci.getId(), razzaFci.getIdGruppo(),
									razzaFci.getIdSezione(), razzaFci.getNome());
							razze.add(razza);
						}
						
					}
					sezione.setRazzaFci(razze);
					sezioni.add(sezione);
					
				}
				
			}
			gruppo.setSezioniFci(sezioni);
			result.add(gruppo);

		}

		return result;
	}
	
	public List<GruppoFCI> selectRazzeFCIWhereIdGruppoIsEqual(final Long idGruppo) {
		List<GruppoFCI> gruppiFci = selectAllGruppiFCI();
		List<SezioneFCI> sezioniFci = selectAllSezioniFCI();
		List<RazzaFCI> razzeFci = selectAllRazzeFCI();

		List<GruppoFCI> result = new ArrayList<>();
		for (GruppoFCI gruppoFci : gruppiFci) {
			
			if (gruppoFci.getId() == idGruppo) {
				GruppoFCI gruppo = new GruppoFCI(gruppoFci.getId(), gruppoFci.getDescrizione());
				List<SezioneFCI> sezioni = new ArrayList<>();
				for (SezioneFCI sezioneFci : sezioniFci) {
					
					if (sezioneFci.getIdGruppo() == gruppo.getId()) {
						SezioneFCI sezione = new SezioneFCI(sezioneFci.getId(), sezioneFci.getIdGruppo(),
								sezioneFci.getDescrizione());

						List<RazzaFCI> razze = new ArrayList<>();
						for (RazzaFCI razzaFci : razzeFci) {
							
							if (razzaFci.getIdGruppo() == gruppo.getId()
									&& razzaFci.getIdSezione().equals(sezioneFci.getId())) {
								RazzaFCI razza = new RazzaFCI(razzaFci.getId(), razzaFci.getIdGruppo(),
										razzaFci.getIdSezione(), razzaFci.getNome());
								razze.add(razza);
							}
							
						}
						sezione.setRazzaFci(razze);
						sezioni.add(sezione);
						
					}
					
				}
				gruppo.setSezioniFci(sezioni);
				result.add(gruppo);
				
			}

		}

		return result;
	}

}
