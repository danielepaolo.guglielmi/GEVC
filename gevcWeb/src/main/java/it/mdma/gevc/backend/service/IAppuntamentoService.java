package it.mdma.gevc.backend.service;

import java.util.Date;
import java.util.List;

import org.springframework.http.ResponseEntity;

import it.mdma.gevc.model.Appuntamento;

public interface IAppuntamentoService {
	public List<Appuntamento> selectAll();
	public Appuntamento insert(Appuntamento agenda);
	public ResponseEntity<?> delete(Long id);
	public List<Appuntamento> selectByDataAppuntamento(Date dataAppuntamento);
    
}