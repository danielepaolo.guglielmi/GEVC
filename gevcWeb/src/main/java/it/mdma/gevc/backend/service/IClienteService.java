package it.mdma.gevc.backend.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import it.mdma.gevc.model.Cliente;
import it.mdma.gevc.model.dto.RegistrationForm;

public interface IClienteService {
	public Cliente insert(RegistrationForm registrationForm);
	
	public List<Cliente> selectAll();
	public Cliente selectById(Long id);
	public Cliente selectByEmail(String email);
	public Cliente insert(Cliente cliente);
	public Cliente update(Long id, Cliente cliente);
	public ResponseEntity<?> delete(Long id);

}
