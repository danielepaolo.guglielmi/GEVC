package it.mdma.gevc.backend.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import it.mdma.gevc.backend.service.IClienteService;
import it.mdma.gevc.model.Cliente;
import it.mdma.gevc.model.dto.RegistrationForm;

@RequestMapping("/cliente")
@Controller
public class ClienteController {

	@Autowired
	private IClienteService clienteService;

	@GetMapping("/getAllClienti")
	public List<Cliente> getAllAzioni() {
		return clienteService.selectAll();

	}

	@GetMapping("/getCliente/{id}")
	public Cliente getCliente(@PathVariable(value = "id") Long id) {
		return clienteService.selectById(id);

	}

	@PostMapping("/registraCliente")
	public void registraCliente(@Valid @ModelAttribute("registrationForm") RegistrationForm registrationForm,
			HttpServletResponse response) throws IOException {
		clienteService.insert(registrationForm);
		response.sendRedirect("/booking");

	}

	@PostMapping("/addCliente")
	public void addCliente(@Valid @ModelAttribute("cliente") Cliente cliente, HttpServletResponse response)
			throws IOException {
		clienteService.insert(cliente);
		response.sendRedirect("/contacts");

	}

	@PostMapping("/updateCliente/{id}")
	public Cliente updateCliente(@PathVariable(value = "id") Long id, @Valid @RequestBody Cliente cliente) {
		return clienteService.update(id, cliente);

	}

	@GetMapping("/deleteCliente/{id}")
	public ResponseEntity<?> deleteCliente(@PathVariable(value = "id") Long id) {
		return clienteService.delete(id);

	}

}
