package it.mdma.gevc.model.dto;

import java.io.Serializable;

public class BookingIntervalStatus implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4887224811672054463L;
	private String data;
	private String intervallo;
	private boolean disponibile;

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getIntervallo() {
		return intervallo;
	}

	public void setIntervallo(String intervallo) {
		this.intervallo = intervallo;
	}

	public boolean isDisponibile() {
		return disponibile;
	}

	public void setDisponibile(boolean disponibile) {
		this.disponibile = disponibile;
	}

	@Override
	public String toString() {
		return "BookingIntervalStatus [data=" + data + ", intervallo=" + intervallo + ", disponibile=" + disponibile
				+ "]";
	}

}
