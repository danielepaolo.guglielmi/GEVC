package it.mdma.gevc.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "appuntamento")
public class Appuntamento implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2461624620324243365L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private Long idCliente;
	private Long idCane;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dataAppuntamento;
	private Long idFasciaOraria;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public Long getIdCane() {
		return idCane;
	}

	public void setIdCane(Long idCane) {
		this.idCane = idCane;
	}

	public Long getIdFasciaOraria() {
		return idFasciaOraria;
	}

	public void setIdFasciaOraria(Long idFasciaOraria) {
		this.idFasciaOraria = idFasciaOraria;
	}

	public Date getDataAppuntamento() {
		return dataAppuntamento;
	}

	public void setDataAppuntamento(Date dataAppuntamento) {
		this.dataAppuntamento = dataAppuntamento;
	}

	@Override
	public String toString() {
		return "Agenda [idCliente=" + idCliente + ", idCane=" + idCane + ", idFasciaOraria=" + idFasciaOraria
				+ ", dataAppuntamento=" + dataAppuntamento + "]";
	}

}
