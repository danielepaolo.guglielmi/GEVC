package it.mdma.gevc.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "razze_fci")
public class RazzaFCI implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3655465868049187091L;
	@Id
	private Long id;
	private Long idGruppo;
	private String idSezione;
	@NotBlank
	private String nome;
	
	public RazzaFCI() {
	}
	
	public RazzaFCI(Long id, Long idGruppo, String idSezione, String nome) {
		this.id = id;
		this.idGruppo = idGruppo;
		this.idSezione = idSezione;
		this.nome = nome;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdGruppo() {
		return idGruppo;
	}

	public void setIdGruppo(Long idGruppo) {
		this.idGruppo = idGruppo;
	}

	public String getIdSezione() {
		return idSezione;
	}

	public void setIdSezione(String idSezione) {
		this.idSezione = idSezione;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "RazzaFCI [id=" + id + ", idGruppo=" + idGruppo + ", idSezione=" + idSezione + ", nome=" + nome + "]";
	}

}
