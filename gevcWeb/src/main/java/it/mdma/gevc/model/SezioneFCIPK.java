package it.mdma.gevc.model;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class SezioneFCIPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6801430265513077159L;
	protected String id;
	protected Long idGruppo;
	
	public SezioneFCIPK() {
	}
	
	public SezioneFCIPK(String id, Long idGruppo) {
		this.id = id;
		this.idGruppo = idGruppo;
	}

}
