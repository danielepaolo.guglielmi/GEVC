package it.mdma.gevc.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "gruppi_fci")
public class GruppoFCI implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 337809349549887060L;
	@Id
	private Long id;
	@NotBlank
	private String descrizione;
	@Transient
	private List<SezioneFCI> sezioniFci;
	
	public GruppoFCI() {
	}
	
	public GruppoFCI(Long id, String descrizione) {
		this.id = id;
		this.descrizione = descrizione;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public List<SezioneFCI> getSezioniFci() {
		return sezioniFci;
	}

	public void setSezioniFci(List<SezioneFCI> sezioniFci) {
		this.sezioniFci = sezioniFci;
	}

	@Override
	public String toString() {
		return "GruppoFCI [id=" + id + ", descrizione=" + descrizione + ", sezioniFci=" + sezioniFci + "]";
	}

}
