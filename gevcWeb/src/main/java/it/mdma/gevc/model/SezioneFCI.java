package it.mdma.gevc.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "sezioni_fci")
public class SezioneFCI implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2587337542560747243L;
	@EmbeddedId
	private SezioneFCIPK sezioneFCIPrimaryKey;
	@NotBlank
	private String descrizione;
	@Transient
	private List<RazzaFCI> razzaFci;
	
	public SezioneFCI() {
	}
	
	public SezioneFCI(String id, Long idGruppo, String descrizione) {
		sezioneFCIPrimaryKey = new SezioneFCIPK(id, idGruppo);
		this.descrizione = descrizione;
	}

	public String getId() {
		return sezioneFCIPrimaryKey.id;
	}

	public void setId(String id) {
		sezioneFCIPrimaryKey.id = sezioneFCIPrimaryKey.id;
	}

	public Long getIdGruppo() {
		return sezioneFCIPrimaryKey.idGruppo;
	}

	public void setIdGruppo(Long idGruppo) {
		sezioneFCIPrimaryKey.idGruppo = sezioneFCIPrimaryKey.idGruppo;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public List<RazzaFCI> getRazzaFci() {
		return razzaFci;
	}

	public void setRazzaFci(List<RazzaFCI> razzaFci) {
		this.razzaFci = razzaFci;
	}

	@Override
	public String toString() {
		return "SezioneFCI [id=" + sezioneFCIPrimaryKey.id + ", idGruppo=" + sezioneFCIPrimaryKey.idGruppo + ", descrizione=" + descrizione + ", razzaFci="
				+ razzaFci + "]";
	}

}
