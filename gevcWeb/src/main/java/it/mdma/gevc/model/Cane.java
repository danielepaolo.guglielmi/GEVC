package it.mdma.gevc.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "cani")
public class Cane implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6470186959654675821L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotBlank
	private String nome;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "razza_id", nullable = true)
	private RazzaFCI razza;
	private String taglia;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dataNascita;
	private String tipoPelo;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cliente_id", nullable = false)
	private Cliente cliente;

	public Cane() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public RazzaFCI getRazza() {
		return razza;
	}

	public void setRazza(RazzaFCI razza) {
		this.razza = razza;
	}

	public String getTaglia() {
		return taglia;
	}

	public void setTaglia(String talia) {
		this.taglia = talia;
	}

	public Date getDataNascita() {
		return dataNascita;
	}

	public void setDataNascita(Date dataNascita) {
		this.dataNascita = dataNascita;
	}

	public String getTipoPelo() {
		return tipoPelo;
	}

	public void setTipoPelo(String tipoPelo) {
		this.tipoPelo = tipoPelo;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	@Override
	public String toString() {
		return "Cane [id=" + id + ", nome=" + nome + ", razza=" + razza + ", talia=" + taglia + ", dataNascita="
				+ dataNascita + ", tipoPelo=" + tipoPelo + "]";
	}

}
