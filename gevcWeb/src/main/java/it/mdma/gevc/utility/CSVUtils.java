package it.mdma.gevc.utility;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import it.mdma.gevc.model.GruppoFCI;
import it.mdma.gevc.model.SezioneFCI;

public class CSVUtils {

	public static List<String[]> getCsvList(final String PATH) {

		List<String[]> result = new ArrayList<>();

		try (Reader reader = Files.newBufferedReader(Paths.get(PATH));
				CSVParser csvParser = new CSVParser(reader,
						CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());) {
			for (CSVRecord csvRecord : csvParser) {

				String[] row = new String[csvRecord.size()];;
				for (int i = 0; i < csvRecord.size(); i++) {
					row[i] = csvRecord.get(i);
				}
				result.add(row);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}

	public static void writeCsvGruppoFCI(List<GruppoFCI> list, final String PATH) throws IOException {
		try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(PATH));
				CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.withHeader("id", "descrizione"));) {

			for (GruppoFCI item : list) {
				csvPrinter.printRecord(item.getId(), item.getDescrizione());
			}

			csvPrinter.flush();
		}
	}

	public static void writeCsvSezioneFCI(List<SezioneFCI> list, final String PATH) throws IOException {
		try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(PATH));
				CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.withHeader("idGruppo", "idSezione", "descrizione"));) {

			for (SezioneFCI item : list) {
				csvPrinter.printRecord(item.getIdGruppo(), item.getId(), item.getDescrizione());
			}

			csvPrinter.flush();
		}
	}

}
