package it.mdma.gevc.utility;

public enum CodiciResponse {

	OK("0"), KO("-1"), NESSUN_RISULTATO("-2"), TROPPI_VALORI("-3"), JSON_INCONSISTENTE("-4"), ERRORE_GENERICO("-5");

	private String codice;

	CodiciResponse(String codice) {
		this.codice = codice;
	}

	public String codice() {
		return codice;
	}

}
