package it.mdma.gevc.utility;

public class ClientPropertyConstants {

	private ClientPropertyConstants() {
	}

	// ***** PROPRIETA' DI CONNESSIONE *****
	public static final String CONNECTION_FILE_PATH = "connection.file.path";
	public static final String BACKEND_BASE_URL = "sisp.backend.url";
	
	public static final String RICERCA_OK = "OK";
	public static final String RICERCA_KO = "KO";
	public static final String RICERCA_NESSUN_RISULTATO = "Nessun Risultato";
	public static final String RICERCA_TROPPE_OCCORRENZE = "Troppe Occorrenze";
	public static final String RICERCA_JSON_INCONSISTENTE = "Json Inconsistente";
	
	public static final String ERRORE_GENERALE = "Errore Generale";
	public static final String SISP_SERVIZIO_NON_DISPONIBILE = "Servizio Non Disponibile";

}
