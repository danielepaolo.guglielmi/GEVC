package it.mdma.gevc.utility;

public class Utils {

	private Utils() {
	}

	public static String leftPad(final String originalString, final int length, final char padCharacter) {
		String paddedString = originalString;
		while (paddedString.length() < length) {
			paddedString = padCharacter + paddedString;
		}
		return paddedString;
	}

}
