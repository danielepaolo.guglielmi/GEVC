package it.mdma.gevc.utility;

public class ConfiguratorConstants {
	
	private ConfiguratorConstants() {
	}
	
	public static final String CONFIGURATION_PATH = "/opt/gevc/file";
	
}
