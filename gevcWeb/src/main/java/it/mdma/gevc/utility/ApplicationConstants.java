package it.mdma.gevc.utility;

public class ApplicationConstants {

	private ApplicationConstants() {
	}
	
	public static final String CONTEXT_GRUPPI_FCI = "gruppi_fci";
	public static final String CONTEXT_SEZIONI_FCI = "sezioni_fci";
	public static final String CONTEXT_RAZZE_FCI = "razze_fci";
	public static final String CONTEXT_FASCE_ORARIE = "fasce_orarie";
	
	public static final String TAGLIA_PICCOLA = "piccola";
	public static final String TAGLIA_MEDIA = "media";
	public static final String TAGLIA_GRANDE = "grande";
	
	public static final String PELO_RASO = "raso";
	public static final String PELO_LUNGO = "lungo";

}
