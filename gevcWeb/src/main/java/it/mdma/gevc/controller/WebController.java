package it.mdma.gevc.controller;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import it.mdma.gevc.model.Appuntamento;
import it.mdma.gevc.model.Cane;
import it.mdma.gevc.model.Cliente;
import it.mdma.gevc.model.dto.RegistrationForm;

@Controller
@RequestMapping("/")
public class WebController extends WebControllerAbstract {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@RequestMapping(value = "login", method = { RequestMethod.GET, RequestMethod.POST })
	public String login(ModelMap model) {

		log.info("login page");

		return "login";
	}

	@RequestMapping(value = "register", method = { RequestMethod.GET, RequestMethod.POST })
	public String register(ModelMap model) {

		log.info("register page");

		model.addAttribute("registrationForm", new RegistrationForm());
		
		return "register";
	}

	@RequestMapping(value = "home", method = { RequestMethod.GET, RequestMethod.POST })
	public String home(ModelMap model) {

		log.info("home page");

		return "home";
	}

	@RequestMapping(value = "contacts", method = { RequestMethod.GET, RequestMethod.POST })
	public String contacts(ModelMap model) {

		log.info("contacts page");

		List<Cliente> clienti = clienteService.selectAll();

		Collections.sort(clienti, new Comparator<Cliente>() {
			public int compare(Cliente c1, Cliente c2) {
				String s1 = c1.getNome() + c1.getCognome();
				String s2 = c2.getNome() + c2.getCognome();
				return s1.compareTo(s2);
			}
		});

		model.addAttribute("clienti", clienti);
		model.addAttribute("cliente", new Cliente());

		return "contacts";
	}

	@RequestMapping(value = "contact", method = { RequestMethod.GET, RequestMethod.POST })
	public String contact(@RequestParam("idCliente") Long idCliente, ModelMap model) {

		log.info("contact page");

		Cliente cliente = clienteService.selectById(idCliente);

		model.addAttribute("cliente", cliente);
		model.addAttribute("cane", new Cane());
		model.addAttribute("taglie", getTaglie());
		model.addAttribute("tipiPelo", getTipoPelo());

		return "contact";
	}
	
	@RequestMapping(value = "booking", method = { RequestMethod.GET, RequestMethod.POST })
	public String booking(Authentication authentication, ModelMap model) {
		
		Cliente cliente = clienteService.selectByEmail(authentication.getName());

		log.info("booking page: " + cliente.getNome() + " " + cliente.getCognome() + " / " + authentication.getAuthorities());

		model.addAttribute("utente", cliente);
		model.addAttribute("appuntamenti", appuntamentoService.selectAll());
		model.addAttribute("fasceOrarie", fasciaOrariaService.selectAll());
		model.addAttribute("appuntamento", new Appuntamento());

		return "booking";
	}
	
	@RequestMapping(value = "calendar", method = { RequestMethod.GET, RequestMethod.POST })
	public String calendar(ModelMap model) {

		log.info("calendar page");
		
		model.addAttribute("fasceOrarie", fasciaOrariaService.selectAll());

		return "calendar";
	}

}
