package it.mdma.gevc.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import it.mdma.gevc.backend.service.IAppuntamentoService;
import it.mdma.gevc.backend.service.ICaneService;
import it.mdma.gevc.backend.service.IClienteService;
import it.mdma.gevc.backend.service.IFasciaOrariaService;
import it.mdma.gevc.backend.service.IGruppoFCIService;
import it.mdma.gevc.backend.service.IRazzaFCIService;
import it.mdma.gevc.backend.service.ISezioneFCIService;

public abstract class WebControllerAbstract {
	
	@Autowired
	protected IAppuntamentoService appuntamentoService;
	
	@Autowired
	protected ICaneService caneService;
	
	@Autowired
	protected IClienteService clienteService;
	
	@Autowired
	protected IFasciaOrariaService fasciaOrariaService;
	
	@Autowired
	protected IGruppoFCIService gruppoFCIService;
	
	@Autowired
	protected ISezioneFCIService sezioneFCIService;
	
	@Autowired
	protected IRazzaFCIService razzaFCIService;
	
	public Map<String, String> getTaglie() {
		Map<String, String> list = new LinkedHashMap<>();
		list.put("Piccola", "Piccola");
		list.put("Media", "Media");
		list.put("Grande", "Grande");
		return list;
	}
	
	public Map<String, String> getTipoPelo() {
		Map<String, String> list = new LinkedHashMap<>();
		list.put("Corto", "Corto");
		list.put("Medio", "Medio");
		list.put("Lungo", "Lungo");
		return list;
	}
	
}
