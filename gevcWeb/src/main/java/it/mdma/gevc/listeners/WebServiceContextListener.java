package it.mdma.gevc.listeners;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.mdma.gevc.model.FasciaOraria;
import it.mdma.gevc.model.GruppoFCI;
import it.mdma.gevc.model.RazzaFCI;
import it.mdma.gevc.model.SezioneFCI;
import it.mdma.gevc.utility.ApplicationConstants;
import it.mdma.gevc.utility.CSVUtils;
import it.mdma.gevc.utility.ConfiguratorConstants;
import it.mdma.gevc.utility.Utils;

public class WebServiceContextListener implements ServletContextListener {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	public WebServiceContextListener() {

	}

	public void contextInitialized(ServletContextEvent servletContextEvent) {
		Long start = System.currentTimeMillis();
		log.info("WebContextListener");

		ServletContext context = servletContextEvent.getServletContext();
		context.setAttribute(ApplicationConstants.CONTEXT_GRUPPI_FCI, getAllGruppiFCI());
		context.setAttribute(ApplicationConstants.CONTEXT_SEZIONI_FCI, getAllSezioniFCI());
		context.setAttribute(ApplicationConstants.CONTEXT_RAZZE_FCI, getAllRazzeFCI());
		context.setAttribute(ApplicationConstants.CONTEXT_FASCE_ORARIE, getAllFasceOrarie());

		log.info("Inizializzazione andata a buon fine in " + (System.currentTimeMillis() - start) + "ms");

	}

	public void contextDestroyed(ServletContextEvent servletContextEvent) {
		// non fa nulla
	}

	private List<GruppoFCI> getAllGruppiFCI() {
		List<GruppoFCI> list = new ArrayList<>();
//		list.add(new GruppoFCI(new Long(1), "Cani da pastore e bovari (esclusi bovari svizzeri)"));
//		list.add(new GruppoFCI(new Long(2), "Cani di tipo Pinscher e Schnauzer - Molossoidi e cani bovari svizzeri"));
//		list.add(new GruppoFCI(new Long(3), "Terrier"));
//		list.add(new GruppoFCI(new Long(4), "Bassotti"));
//		list.add(new GruppoFCI(new Long(5), "Cani di tipo Spitz e di tipo primitivo"));
//		list.add(new GruppoFCI(new Long(6), "Segugi e cani per pista di sangue"));
//		list.add(new GruppoFCI(new Long(7), "Cani da ferma"));
//		list.add(new GruppoFCI(new Long(8), "Cani da riporto - Cani da cerca - Cani da acqua"));
//		list.add(new GruppoFCI(new Long(9), "Cani da compagnia"));
//		list.add(new GruppoFCI(new Long(10), "Levrieri"));
//		try {
//			CSVUtils.writeCsvGruppoFCI(list, ConfiguratorConstants.CONFIGURATION_PATH + "/gruppi_fci.csv");
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		List<String[]> gruppi = CSVUtils.getCsvList(ConfiguratorConstants.CONFIGURATION_PATH + "/gruppi_fci.csv");
		for (String[] gruppo : gruppi) {
			GruppoFCI item = new GruppoFCI(Long.parseLong(gruppo[0]), gruppo[1]);
			list.add(item);
		}
		
		return list;

	}

	private List<SezioneFCI> getAllSezioniFCI() {
		List<SezioneFCI> list = new ArrayList<>();
//		list.add(new SezioneFCI("1a", new Long(1), "Cani da pastore"));
//		list.add(new SezioneFCI("2a", new Long(1), "Cani da bovari (esclusi bovari svizzeri)"));
//		list.add(new SezioneFCI("1a", new Long(2), "Tipo Pinscher e Schnauzer"));
//		list.add(new SezioneFCI("2a", new Long(2), "Molossoidi"));
//		list.add(new SezioneFCI("3a", new Long(2), "Bovari Svizzeri"));
//		list.add(new SezioneFCI("1a", new Long(3), "Terrier di taglia grande e media (gamba lunga)"));
//		list.add(new SezioneFCI("2a", new Long(3), "Terrier di piccola taglia (gamba corta)"));
//		list.add(new SezioneFCI("3a", new Long(3), "Terrier di tipo bull (gamba lunga)"));
//		list.add(new SezioneFCI("4a", new Long(3), "Terrier di compagnia (gamba corta)"));
//		list.add(new SezioneFCI("1", new Long(4), "NO_SECTION"));
//		list.add(new SezioneFCI("1a", new Long(5), "Cani nordici da slitta"));
//		list.add(new SezioneFCI("2a", new Long(5), "Cani nordici da caccia"));
//		list.add(new SezioneFCI("3a", new Long(5), "Cani nordici da guardia e da pastore"));
//		list.add(new SezioneFCI("4a", new Long(5), "Spitz Europei"));
//		list.add(new SezioneFCI("5a", new Long(5), "Spitz asiatici e razze affini"));
//		list.add(new SezioneFCI("6a", new Long(5), "Tipo primitivo"));
//		list.add(new SezioneFCI("7a", new Long(5), "Tipo primitivo da caccia"));
//		list.add(new SezioneFCI("8a", new Long(5), "Tipo primitivo da caccia con cresta sul dorso"));
//		list.add(new SezioneFCI("1a", new Long(6), "Segugi"));
//		list.add(new SezioneFCI("2a", new Long(6), "Cani per pista di sangue"));
//		list.add(new SezioneFCI("3a", new Long(6), "Razze affini"));
//		list.add(new SezioneFCI("1a", new Long(7), "Cani da ferma continentali"));
//		list.add(new SezioneFCI("2", new Long(7), "Cani da ferma britannici ed irlandesi"));
//		list.add(new SezioneFCI("1a", new Long(8), "Cani da riporto"));
//		list.add(new SezioneFCI("2a", new Long(8), "Cani da cerca"));
//		list.add(new SezioneFCI("3a", new Long(8), "Cani da acqua"));
//		list.add(new SezioneFCI("1a", new Long(9), "Bichons e affini"));
//		list.add(new SezioneFCI("2a", new Long(9), "Barboni"));
//		list.add(new SezioneFCI("3a", new Long(9), "Cani belgi di piccola taglia"));
//		list.add(new SezioneFCI("4a", new Long(9), "Cani nudi"));
//		list.add(new SezioneFCI("5a", new Long(9), "Cani del Tibet"));
//		list.add(new SezioneFCI("6a", new Long(9), "Chihuahua"));
//		list.add(new SezioneFCI("7a", new Long(9), "Spaniel inglesi di compagnia"));
//		list.add(new SezioneFCI("8a", new Long(9), "Spaniel giapponesi e pechinesi"));
//		list.add(new SezioneFCI("9a", new Long(9), "Spaniel nani continentali"));
//		list.add(new SezioneFCI("10a", new Long(9), "Kromfohrländer"));
//		list.add(new SezioneFCI("11a", new Long(9), "Molossoidi di piccola taglia"));
//		list.add(new SezioneFCI("1a", new Long(10), "Levrieri a pelo lungo o frangiato"));
//		list.add(new SezioneFCI("2a", new Long(10), "Levrieri a pelo duro"));
//		list.add(new SezioneFCI("3a", new Long(10), "Levrieri a pelo corto"));		
//		try {
//			CSVUtils.writeCsvSezioneFCI(list, ConfiguratorConstants.CONFIGURATION_PATH + "/sezioni_fci.csv");
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		List<String[]> sezioni = CSVUtils.getCsvList(ConfiguratorConstants.CONFIGURATION_PATH + "/sezioni_fci.csv");
		for (String[] sezione : sezioni) {
			SezioneFCI item = new SezioneFCI(sezione[1], Long.parseLong(sezione[0]), sezione[2]);
			list.add(item);
		}

		
		return list;

	}

	private List<RazzaFCI> getAllRazzeFCI() {
		List<RazzaFCI> list = new ArrayList<>();
		List<String[]> razze = CSVUtils.getCsvList(ConfiguratorConstants.CONFIGURATION_PATH + "/razze_fci.csv");
		for (String[] razza : razze) {
			RazzaFCI item = new RazzaFCI(Long.parseLong(razza[0]), Long.parseLong(razza[1]), razza[2], razza[3]);
			list.add(item);
		}
		return list;

	}

	private List<FasciaOraria> getAllFasceOrarie() {
		List<FasciaOraria> list = new ArrayList<>();
		int i;
		
		try {
			for (i = 8; i < 20; i++) {
				
				Date start1 = new SimpleDateFormat("HH:mm:ss").parse(Utils.leftPad(Integer.toString(i), 2, '0') + ":30:00");
				Date end1 = new SimpleDateFormat("HH:mm:ss").parse(Utils.leftPad(Integer.toString(i + 1), 2, '0') + ":00:00");
				list.add(new FasciaOraria(start1, end1));
				Date start2 = new SimpleDateFormat("HH:mm:ss").parse(Utils.leftPad(Integer.toString(i + 1), 2, '0') + ":00:00");
				Date end2 = new SimpleDateFormat("HH:mm:ss").parse(Utils.leftPad(Integer.toString(i + 1), 2, '0') + ":30:00");
				list.add(new FasciaOraria(start2, end2));
			}
			return list;
			
		} catch (ParseException e) {
			return null;
			
		}

	}

}